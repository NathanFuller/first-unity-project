﻿using UnityEngine;
using System.Collections;

// NATHAN FULLER

public class MoveBehaviourScript : MonoBehaviour {

	public float speed = 1.0f;

	private Transform thisTransform;
	private Vector3 moveVector;

	// Use this for initialization
	void Awake (){
		thisTransform = transform;
		moveVector = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		moveVector = Vector3.zero;
		bool moving = false;

		if (Input.GetKey (KeyCode.RightArrow)) {
			if (thisTransform.localScale.x < 0)
				thisTransform.localScale = new Vector3(thisTransform.localScale.x * -1, 
														thisTransform.localScale.y, 0);
			moveVector.x += 5.0f;
			moving = true;
		}

		if (Input.GetKey (KeyCode.LeftArrow)) {
			if (thisTransform.localScale.x > 0)
				thisTransform.localScale = new Vector3(thisTransform.localScale.x * -1, 
														thisTransform.localScale.y, 0);
			moveVector.x -= 5.0f;
			moving = true;
		}

		move (moveVector);
	}

	private void move(Vector3 moveVector) {
		moveVector *= speed * Time.deltaTime;
		thisTransform.position = thisTransform.position + moveVector;
	}
}
